FROM httpd:2.4-alpine
RUN apk add --update nano
RUN apk add --no-cache tzdata
ENV TZ America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY ./usr_local_apache2_conf/* /usr/local/apache2/conf/
COPY ./usr_local_apache2_conf_extra/* /usr/local/apache2/conf/extra/
COPY ./usr_local_apache2_htdocs/* /usr/local/apache2/htdocs/
COPY ./port_80_html/index.html /usr/local/apache2/piveraproject/


